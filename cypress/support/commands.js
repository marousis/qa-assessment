// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add('login', (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })
Cypress.Commands.add("findPostWithColumnId", (id) => {
  cy.get(`:nth-child(${id}) > .column-title`);
});
Cypress.Commands.add("findCheckboxWithColumnId", (id) => {
  cy.get(
    `.MuiTableBody-root > :nth-child(${id}) > .MuiTableCell-paddingCheckbox > .MuiButtonBase-root > .MuiIconButton-label > .PrivateSwitchBase-input-88`
  );
});
Cypress.Commands.add("findSelectionWordingWithId", (id) => {
  if (id < 2) {
    cy.contains(`${id} item selected`);
  } else {
    cy.contains(`${id} items selected`);
  }
});
Cypress.Commands.add("findUserWithColumnId", (id) => {
  cy.get(`:nth-child(${id}) > .column-id`);
});
Cypress.Commands.add("findUserWithMail", (mail) => {
  cy.get(`:nth-child(${mail}) > .column-email`);
});
Cypress.Commands.add("findUserWithUsername", (username) => {
  cy.get(`:nth-child(${username}) > .column-username`);
});
Cypress.Commands.add("findDeleteWording", (i) => {
  if (i < 2) {
    cy.contains("Element deleted");
  } else {
    cy.contains(`${i} elements deleted`);
  }
});
