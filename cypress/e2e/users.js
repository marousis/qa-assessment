// Locators
const users_tab = "Users"; // Use with cy.contains
const save_btn = "Save"; // Use with cy.contains
const id = "#id";
const name = "#name";
const username = "#username";
const mail = "#email";
const street_address = "#address\\.street";
const phone = "#phone";
const site = "#website";
const company = "#company\\.name";
const delete_btn = "Delete"; // Use with cy.contains

// Data
const users_path = "#/users";
const id_new_value = "2";
const name_new_value = "panagiotis";
const username_new_value = "marousis";
const mail_new_value = "marousis@gmail.com";
const street_address_new_value = "p. delta 32";
const phone_new_value = "6972007999";
const site_new_value = "google.gr";
const company_new_value = "mcompany";
const max = 3;

//Loop list
var user = [
  { locator: id, value: id_new_value },
  { locator: name, value: name_new_value },
  { locator: username, value: username_new_value },
  { locator: mail, value: mail_new_value },
  { locator: street_address, value: street_address_new_value },
  { locator: phone, value: phone_new_value },
  { locator: site, value: site_new_value },
  { locator: company, value: company_new_value },
];

context("Interactions with users", () => {
  beforeEach(() => {
    cy.visit(Cypress.env("baseUrl"));
    cy.contains(users_tab).click();
    cy.url().should("eq", Cypress.env("baseUrl") + users_path);
  });
  it("Edit a user", () => {
    cy.findUserWithColumnId(1).click();
    cy.contains(save_btn).should("be.disabled");
    user.forEach(async function (field) {
      cy.get(field.locator).clear().type(field.value);
    });
    cy.contains(save_btn).should("be.enabled").click();
    cy.findUserWithColumnId(2)
      .siblings()
      .should((line) => {
        user.forEach(async function (field) {
          expect(line).to.contain(field.value);
        });
      });
  });
  it("Delete one user", () => {
    cy.findUserWithUsername(1)
      .click()
      .then((el) => {
        cy.contains(delete_btn).click();
        cy.findDeleteWording(1).should("be.visible");
        cy.contains(el.text()).should("not.exist");
      });
  });
  it("Delete multiple users", () => {
    for (let i = 1; i < max; i++) {
      cy.findUserWithMail(i).then((el) => {
        cy.contains(el.text()).as(`text${i}`);
        cy.findCheckboxWithColumnId(i).click();
        cy.findSelectionWordingWithId(i).should("be.visible");
      });
    }
    cy.contains(delete_btn).click({ force: true });
    cy.findDeleteWording(max - 1).should("be.visible");
    for (let i = 1; i < max; i++) {
      cy.contains(`@text${i}`).should("not.exist");
    }
  });
});
