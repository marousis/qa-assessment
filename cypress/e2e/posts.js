// Locators
const user_id = "#userId";
const new_user_id = '[data-value="2"]';
const post_id = "#id";
const post_title = "#title";
const post_body = "#body";
const save_btn = "Save"; // Use with cy.contains
const delete_btn = "Delete"; // Use with cy.contains

// Data
const posts_path = "#/posts";
const new_post_title = "marousis";
const new_post_body = "panagiotis";
const new_user_id_value = "2";
const new_ticket_id = "2";
const max = 3;

//Loop list
var post = [
  { locator: post_id, value: new_ticket_id },
  { locator: post_title, value: new_post_title },
  { locator: post_body, value: new_post_body },
];

context("User's interactions with posts", () => {
  beforeEach(() => {
    cy.visit(Cypress.env("baseUrl"));
    cy.url().should("eq", Cypress.env("baseUrl") + posts_path);
  });
  it("Edit a post", () => {
    cy.findPostWithColumnId(1).click();
    cy.contains(save_btn).should("be.disabled");
    cy.get(user_id).should("be.visible").click();
    cy.get(new_user_id).click();
    post.forEach(async function (field) {
      cy.get(field.locator).clear().type(field.value);
    });
    cy.contains(save_btn).should("be.enabled").click();
    cy.contains(new_ticket_id)
      .parent()
      .siblings()
      .should((line) => {
        post.forEach(async function (field) {
          expect(line).to.contain(field.value);
        });
      });
  });
  it("Delete one post", () => {
    cy.findPostWithColumnId(1).then((el) => {
      cy.findCheckboxWithColumnId(1).click();
      cy.findSelectionWordingWithId(1).should("be.visible");
      cy.contains(delete_btn).click();
      cy.findDeleteWording(1).should("be.visible");
      cy.contains(el.text()).should("not.exist");
    });
  });
  it("Delete multiple posts", () => {
    for (let i = 1; i < max; i++) {
      cy.findPostWithColumnId(i).then((el) => {
        cy.contains(el.text()).as(`text${i}`);
        cy.findCheckboxWithColumnId(i).click();
        cy.findSelectionWordingWithId(i).should("be.visible");
      });
    }
    cy.contains(delete_btn).click();
    cy.findDeleteWording(max - 1).should("be.visible");
    for (let i = 1; i < max; i++) {
      cy.contains(`@text${i}`).should("not.exist");
    }
  });
});
