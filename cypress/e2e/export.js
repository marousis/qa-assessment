// Locators
const export_btn = "Export"; // Use with cy.contains
const users_tab = "Users"; // Use with cy.contains

// Data
const posts_path = "#/posts";
const users_path = "#/users";

context("Export functionality", () => {
  beforeEach(() => {
    cy.visit(Cypress.env("baseUrl"));
  });
  afterEach(() => {
    cy.exec("cd " + "cypress/downloads/" + " " + "&& rm -rf *", {
      failOnNonZeroExit: false,
    });
  });
  it("Export posts and validate", () => {
    cy.url().should("eq", Cypress.env("baseUrl") + posts_path);
    cy.contains(export_btn).should("be.visible").click({ force: true });
    cy.readFile("cypress/downloads/posts.csv").should("exist");
    cy.readFile("cypress/downloads/posts.csv").then((data2) =>
      cy
        .readFile("cypress/fixtures/posts_default.csv")
        .should("deep.equal", data2)
    );
  });
  it("Export users and validate", () => {
    cy.contains(users_tab).click();
    cy.url().should("eq", Cypress.env("baseUrl") + users_path);
    cy.contains(export_btn).should("be.visible").click({ force: true });
    cy.readFile("cypress/downloads/users.csv").should("exist");
    cy.readFile("cypress/downloads/users.csv").then((data2) =>
      cy
        .readFile("cypress/fixtures/users_default.csv")
        .should("deep.equal", data2)
    );
  });
});
