import * as React from "react";
import { Admin, Resource, EditGuesser} from 'react-admin';
import jsonServerProvider from 'ra-data-json-server';

import { UserList} from "./users"
import { PostList } from "./posts"

const dataProvider = jsonServerProvider('https://jsonplaceholder.typicode.com');

const App = () => (
    <Admin dataProvider={dataProvider}>
      <Resource name="posts" list={PostList} edit={EditGuesser} />
      <Resource name="users" list={UserList} edit={EditGuesser} />
    </Admin>
);

export default App;
