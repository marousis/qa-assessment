# Getting Started

## Install dependencies
```bash
npm install 
```
##  Start the development server
Run the following command and the development server will start running on http://localhost:3000.
```bash
npm run start 
```


##  Open Cypress Test Runner
By runnig this command you will be able to run your tests on the Cypress Test Runner. 

```bash
npm run cypress:open 
```
## Tasks

- Write as many tests as you like using the [Cypress framework](https://cypress.io), which has already been configured to run in this app. 
- Make sure to cover the following cases: 
  - Check that a user can successfully edit a Post 
  - Check that a user can successfully delete a Post 
  - Check that a user can successfully edit a User 
  - Check that a user can successfully delete a User 
  - Check that a user can successfully delete multiple Posts
  - Check that a user can successfully delete multiple Users
- You can test whatever you'd like - a mix of tests that are designed to pass or designed to fail.
- Write all your tests in `cypress/e2e` folder. 

### Submission

- Fork this repository, commit your changes and as soon as you are ready send it back to us via an email.
- Please feel free to contact us in case you have any questions about the assessment.




